from django.db import models
from django.db.models import Model
import uuid

class News(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	CATEGORIES = (
		(1, "Aktuell"),
		(2, "Update"),
		(3, "wichtige Mitteilung"),
		(4, "anderes")
	)
	title = models.CharField(max_length=100)
	category = models.IntegerField(choices=CATEGORIES)
	content = models.CharField(max_length=900)