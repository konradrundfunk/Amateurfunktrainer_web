from random import shuffle

from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

from backend.frontend.models import News
from backend.questions.models import Question, Chapter, Progress


def index(request, file = ""):
	if file == "robots.txt":
		return robots(request)
	
	news = News.objects.all().values()
	stats = len(Question.objects.all())

	for i in news:
		i["category"] = News.CATEGORIES[i["category"]][1]


	state = {
		"news" : news,
		"fragen" : stats
	}
	if file == "":
		return render(request, "index.html", state)
	else:
		return render(request, str(file), state)


def profile(request):
	if request.method == "POST":
		request.session.modified = True
		
		# reset tracking
		request.session['questions'] = {}
		request.session['next'] = 0
		request.session['res'] = []
		
		#set new question catalogue
		request.session['questions'] = serializers.serialize('json', Question.objects.filter(
			chaptername__chaptername=request.POST.get("chapter")))
		return redirect("/question/")

	if request.user.is_authenticated:
		chapter = Chapter.objects.all().order_by("category")
		news = News.objects.all().values()
		for i in news:
			i["category"] = News.CATEGORIES[i["category"]][1]
		
		state = {
			"username": request.user.username,
			"chapters": chapter,
			"news": news,
		}
		return render(request, "profile/index.html",
		              state)  # HttpResponse(loader.get_template('profile/index.html').render(state))
	else:
		return redirect("/login")

def result(request):
	if request.user.is_authenticated:
		if request.method == "POST":
			question = request.session['questions']
			try:
				answer_given = request.POST.get("options")
				result = request.session['res']
				next = request.session['next']
			except(KeyError):
				next = 0
				result = []
			
			y = 0
			for i in serializers.deserialize("json", question):
				#print(next)
				if next == y:
					current_question = i.object
					break
				y = y + 1
			#print(answer_given)
			if current_question.answer0 == answer_given:
				state = {
					"correct" : True,
					"answer_given": answer_given,
					"correct_answer": current_question.answer0,
					"id" : current_question.id
				}
			else:
				state = {
					"correct": False,
					"answer_given": answer_given,
					"correct_answer" : current_question.answer0,
					"id": current_question.id
				}
				
			return render(request, "question/result.html", state)
		else:
			return redirect("/question/")
	else:
		return redirect("/login")
	
	
def question(request):
	global old_progress
	if request.user.is_authenticated:
		if request.method == "POST":
			correct = bool(request.POST.get("correct"))
			print(correct)
			id = request.POST.get("id")
			next = request.session['next'] + 1
			request.session['next'] = next
			
			try:
				print(Question.objects.filter(id__contains=id)[0])
				old_progress = Progress.objects.get(question_id=Question.objects.filter(id__contains=id)[0], user=request.user)
				print(old_progress)
				
				if correct:
					old_progress.times_answered = old_progress.times_answered + 1
				else:
					if old_progress.difficulty <= 1:
						old_progress.difficulty = 1
					elif old_progress.difficulty >= 4:
						old_progress.difficulty = 4
					else:
						old_progress.difficulty = old_progress.difficulty + 1
				
				
				old_progress.save()
				
			except (NameError, ObjectDoesNotExist):
				Progress.objects.create(question_id=Question.objects.filter(id__contains=id)[0],
				                        category=3, user=request.user, difficulty=1, marked=False, times_answered=1,
				                        last_answered=timezone.now())
			
		try:
			question = request.session['questions']
			try:
				next = request.session['next']
			except(KeyError):
				next = 0
				
			list = []
			for i in serializers.deserialize("json", question):
				list.append(i.object)
	
			answers = [list[next].answer0, list[next].answer1, list[next].answer2, list[next].answer3]
			
			shuffle(answers)
			
			list[next].answer0 = answers[0]
			list[next].answer1 = answers[1]
			list[next].answer2 = answers[2]
			list[next].answer3 = answers[3]
			
			state = {
				"question": list[next]
			}
			
			return render(request, 'question/question.html', state)
	
		except(KeyError, AttributeError, IndexError):
			return redirect("/accounts/profile/")
	else:
		return redirect("/login")
		
def robots(request):
	return HttpResponse("User-agent: * Disallow: /admin")