from django.contrib import admin
from backend.questions.models import Question, Progress, Chapter

admin.site.register(Question)
admin.site.register(Progress)
admin.site.register(Chapter)

# Register your models here.
