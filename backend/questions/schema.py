import graphene
from graphene_django import DjangoObjectType

from backend.questions.models import Question

class QuestionSchema(DjangoObjectType):
    class Meta:
        model = Question
        fields = ("id", "question", "category", "answer0", "answer1", "answer2", "answer3")


class Query(graphene.ObjectType):
    example = graphene.String(default_value="working!")
    
    all_questions = graphene.List(QuestionSchema)
    
    def resolve_all_questions(self, args):
        return Question.objects.all()
    
    def resolve_questions_by_id(self, info, id):
        try:
            return Question.objects.get(id=id)
        except Question.DoesNotExist:
            return None


schema = graphene.Schema(query=Query)
