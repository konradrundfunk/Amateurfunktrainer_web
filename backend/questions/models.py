import uuid

from django.contrib.auth.models import User

from django.db import models

class Chapter(models.Model):
    CATEGORIES = (
        (1, "Technik Klasse A"),
        (2, "Technik Klasse E"),
        (3, "Betriebstechnik"),
        (4, "Vorschriften")
    )
    category = models.IntegerField(choices=CATEGORIES)

    chaptername = models.TextField(primary_key=True)
    
    def __str__(self):
        return str(self.chaptername)

class Question(models.Model):
    CATEGORIES = (
        (1, "Technik Klasse A"),
        (2, "Technik Klasse E"),
        (3, "Betriebstechnik"),
        (4, "Vorschriften")
    )
    id = models.TextField(primary_key=True)
    question = models.TextField()
    
    category = models.IntegerField(choices=CATEGORIES)
    chaptername = models.ForeignKey(Chapter, on_delete=models.CASCADE, null=True)
    
    
    answer0 = models.TextField()
    answer1 = models.TextField()
    answer2 = models.TextField()
    answer3 = models.TextField()
    
    def __str__(self):
        return str(self.id)

class Progress(models.Model):
    DIFFICULTY = (
        (1, "easy"),
        (2, "medium"),
        (3, "challenging"),
        (4, "hard")
    )
    CATEGORIES = (
        (1, "Technik Klasse A"),
        (2, "Technik Klasse E"),
        (3, "Betriebstechnik"),
        (4, "Vorschriften")
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)

    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    category = models.IntegerField(choices=CATEGORIES)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    difficulty = models.IntegerField(choices=DIFFICULTY)
    marked = models.BooleanField(max_length=1)
    times_answered = models.IntegerField()
    last_answered = models.DateTimeField()

    def __str__(self):
        return str(self.user) + str(self.question_id)
    