# Amateurfunktrainer

## Setup
First of all you need pip and python3 to get up and running with this project. If you aren't familular with thoose tools just use the [funktrainer.app](https://funktrainer.app). When installed pip and python3 install a few dependencies in order to get running with django. You have to install the following packages from pip with `pip install <package>`. 
1. django
2. graphene_django
3. django-admin # and add it to your path


Now it depends on your database for a simpler setup we just use sqlite3 so no further dependencies are required. 
To get going with django just set your database configured with a table for Amateurfunktrainer in the settings.py [see](https://docs.djangoproject.com/en/4.0/ref/databases/) and test it out using `django-admin check --database <db name from settings.py>`. Than run the migrations with the manage.py file as follows `python manage.py makemigrations`. 
To load the questiondata use the `django-admin loaddata "path to the data/out.json"`. 
To get a user with acces the admin pannel run `python manage.py createsuperuser`.
Than you can finally run the server with `python3 manage.py runserver` get the url and open it in your webbrowser and go the the Endpoints or just browse around.

## Endpoints
- /api/ # An graphql endpoint for the questions open to everyone
- /graphql/ # Play around with the endpoints 
- /login/ # Login with the user created above
- /accounts/profile/ # See a overview of all chapters and the current news that have beed added in the admin pannel 
- /question/ # question itself gets it via the state so redirects if the state is none 
- /result/ # also works via state so should also redirect if none 

If you got any questions or whant to imporove my software please open an issue or a PR :).
